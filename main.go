package main

import (
	"gin_demo/utils"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func main() {
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.String(200, "Hello, gin")
	})

	r.POST("/sum", func(c *gin.Context) {
		limit := c.PostForm("limit")
		limitInt, err := strconv.ParseInt(limit, 10, 64)
		if err != nil {
			return
		}
		if limitInt > 6 || limitInt <= 0 {
			return
		}
		a := utils.GetRandNum(limitInt)
		b := utils.GetRandNum(limitInt)
		c.JSON(http.StatusOK, gin.H{
			"rand_num1": a,
			"rand_num2": b,
			"sum":       utils.GetSum(a, b),
		})
	})
	// listen and serve on 0.0.0.0:8080
	err := r.Run()
	if err != nil {
		return
	}
}
